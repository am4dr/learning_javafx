package javafx_tutorial;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Sample1 extends Application {
    /*
     * JavaFXのパッケージング機能によってjar化する時にはランチャが自動的に含まれるため必要ない。
     * そのほかの場合には必要。
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * JavaFXアプリケーションのエントリポイント
     * 
     * @param primaryStage アプリケーションのトップレベルコンテナ
     */
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        Button button = new Button();
        button.setText("Say 'Hello'");

        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World");
            }
        });
        // StackPaneはScene内のコンテンツ階層のroot。
        StackPane root = new StackPane();
        root.getChildren().add(button);

        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}
