package javafx_tutorial;

import java.util.ArrayList;
import java.util.List;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TutorialSampleLauncher extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private class Sample {
        final Class<? extends Application> className;
        final String label;

        Sample(Class<? extends Application> className, String label) {
            this.className = className;
            this.label = label;
        }
    }

    @Override
    public void start(final Stage primaryStage) {
        primaryStage.setTitle("sample launcher");
        List<Sample> sampleList = new ArrayList<>();
        sampleList.add(new Sample(Sample1.class,
                "Hello World, JavaFX Style"));
        sampleList.add(new Sample(Sample2.class,
                "Form Design in JavaFX"));
        sampleList.add(new Sample(Sample3.class,
                "Fancy Design with CSS"));
        sampleList.add(new Sample(Sample4.class,
                "User Interface Design with FXML"));
        sampleList.add(new Sample(Sample5.class,
                "Animated Shapes and Visual Effects"));

        VBox root = new VBox(5);
        root.setAlignment(Pos.CENTER);
        for (final Sample sample : sampleList) {
            Button button = new Button();
            button.setText(sample.label);
            button.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    callStartMethod(sample.className, primaryStage);
                }
            });
            root.getChildren().add(button);
        }
        primaryStage.setScene(new Scene(root, 300, 200));
        primaryStage.show();
    }

    /**
     * 対象クラスのstart()を借りてこのアプリケーションのSceneを差し替える。
     * この実装はたぶん良くないだろうと思う。
     */
    private void callStartMethod(Class<? extends Application> c, Stage stage) {
        try {
            Application application = c.newInstance();
            application.start(stage);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
