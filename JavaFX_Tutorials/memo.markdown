Getting Started with JavaFX
===========================

[Getting Started with JavaFX](http://docs.oracle.com/javafx/2/get_started/jfxpub-get_started.htm)

1. Hello World, JavaFX Style
----------------------------
+ javafx.application.Application#start()がJavaFXアプリケーションのエントリポイント
+ Sceneはコンテナ。Stageはトップレベルコンテナ。
+ Sceneのコンテンツは階層構造になっている。rootはPane。rootはSceneのサイズに自動的に変化する。
+ JavaFXのパッケージング機能はランチャをjarに含めるのでmain()は必須でない。
    + それ以外の場合にはmain()から起動してやる必要がある。
+ イベントはEventHandler<T extends Event>#handle(T)で扱う。

2.  Form Design in JavaFX
-------------------------
+ Fontで指定しているけどスタイルシート使うとベターらしい。
+ `grid.setGridLinesVisible(true)`でグリッドが表示できてデバッグに便利。
+ firebrickは耐火煉瓦の意。
+ 新出レイアウトGridPane, HBox。

3. Fancy Design with CSS
------------------------
+ SceneにスタイルシートへのURLを追加する。
+ CSSでの.rootはSceneのルートノードということ。
+ javaコード上で`対象.setId("id名")`してCSSから参照できる。
+ ボタンの.buttonと.botton:hoverみたいにコンポーネントの状態ごとにidがあったりするぽい。

4. User Interface  Design with FXML
-----------------------------------
+ fxmlファイルとController.javaを追加。
+ fxmlファイルを読み込んでParentを生成。ParentからSceneを作成。
+ コントローラからfxml内の要素を参照するためにfx:id="なまえ"プロパティをつける。
+ コントローラはトップレベルのレイアウトに`fx:controller="クラス名"`とプロパティで指定する。
+ `#なまえ`でコントローラの公開された要素を参照することができる。
+ @FXMLアノテーションによって非公開要素をfxmlのコンテクストで参照できるようにできるぽい。
+ じつはコントローラクラスを作らなくてもfxml内にスクリプトかける。JSR 223。
    + `<?language 言語名?>`をxmlのdoctype宣言の後にかく。javascript, groovy, jython, clojure...
    + onActionなどで使うときにはスクリプトを呼び出す形でかく。仮引数にeventとかかく。
    + <fx:script>の中に関数書いたり`source=""`プロパティでファイル指定したり。
+ `styleClass="なまえ"`プロパティで要素にCSS用にクラスを追加できる。
+ fx:idとidの違いはなんなんだ。`fx:welcome-text`はエラーになった。たぶん'-'のせい。


5. Animated Shapes and Visual Effects
-------------------------------------
+ 図形(Shape)はGroupによってまとめられ、Groupによる階層構造を成すことができる。
+ JavaFXのプロパティは他のプロパティにbind()でバインドすることができる。
+ BlendModeによって重なり時のブレンド方法を指定することができる。
+ Timelineによってアニメーションを実現できる。KeyFrameを追加してTimeline#start()。
+ KeyFrameには時刻とKeyValueを記録する。KayValueは対象プロパティとその値からなる。

6. Deployment Quickstart
------------------------
+ NetBeansの例だった。。。
+ EclipseではExport->runnable jar->extractするかpackageするか選択。
+ 実行は`javaw -jar 出力された.jar`でできる。
+ javafxpackagerというもので環境にあったインストーラを作成することができるようだ。
